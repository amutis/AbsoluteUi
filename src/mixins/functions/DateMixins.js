export default {
  methods: {
    formated_date: function (date) {
      if (this.$moment(date).format('YYYY-MM-DD') === this.$moment(Date(new Date())).format('YYYY-MM-DD')) {
        return 'Today | ' + this.$moment(Date(date)).format('YYYY-MM-DD')
      } else {
        return this.$moment(date).format('YYYY-MM-DD')
      }
    },
    cut_off: function (date) {
      if (this.$moment(date).format('HH:mm') > this.$moment('11:00', 'HH:mm').format('HH:mm')) {
        console.log(true)
        return true
      } else {
        console.log(false)
        return false
      }
    },
    cut_off2: function (date) {
      if (this.$moment(date).format('YYYY-MM-DD') <= this.$moment(Date(new Date())).format('YYYY-MM-DD')) {
        return true
      } else if (this.$moment(date).subtract(1, 'days').format('YYYY-MM-DD') <= this.$moment(Date(new Date())).format('YYYY-MM-DD') && this.$moment(Date(new Date())).format('HH:mm') > this.$moment('11:00', 'HH:mm').format('HH:mm')) {
        return true
      } else {
        return false
      }
    },
    due_by: function (date) {
      return this.$moment(date).add(30, 'days').format('YYYY-MM-DD')
    },
    format: function (n) {
      return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
    }
  }
}
