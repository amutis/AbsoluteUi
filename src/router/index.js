import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'

// Modules
import Admin from '@/components/Modules/Admin/Admin'
import AdminDashboard from '@/components/Modules/Admin/Dashboard'

import MCompany from '@/components/Modules/Company/Company'
import CompanyDashboard from '@/components/Modules/Company/Dashboard'

// Rides
import CreateTrip from '@/components/Rides/CreateTrip'
import AllTrips from '@/components/Rides/AllTrips'
import AssignedTrips from '@/components/Rides/AssignedTrips'
import CompletedTrips from '@/components/Rides/CompletedTrips'
import UnAssignedTrips from '@/components/Rides/UnAssignedTrips'
import TripsInSession from '@/components/Rides/TripsInSession'
import BlockBooking from '@/components/Rides/BlockBooking'
import EditTrip from '@/components/Rides/EditTrip'
import Notes from '@/components/Rides/Notes'

// Schedule
import FullSchedule from '@/components/Schedules/FullSchedule'
import DriverSchedule from '@/components/Schedules/DriverSchedule'

// Driver
import AddDriver from '@/components/Drivers/AddDriver'
import Drivers from '@/components/Drivers/Drivers'
import Driver from '@/components/Drivers/Driver'

// Vehicles
import Vehicle from '@/components/Vehicles/Vehicle'
import AssignVehicle from '@/components/Vehicles/AssignVehicle'
import Vehicles from '@/components/Vehicles/Vehicles'
import AddVehicle from '@/components/Vehicles/AddVehicle'

// Vehicle Owners
import AddOwner from '@/components/VehicleOwners/AddOwner'
import Owner from '@/components/VehicleOwners/Owner'
import Owners from '@/components/VehicleOwners/Owners'

// Assignments
import Assignments from '@/components/Assignments/AllAssignments'

// Companies
import AddCompany from '@/components/Institutions/AddCompany'
import Companies from '@/components/Institutions/Companies'
import Company from '@/components/Institutions/Company'
import Contracts from '@/components/Institutions/Contracts'
import AddContract from '@/components/Institutions/AddContract'
import EditContract from '@/components/Institutions/EditContract'

// Users
import DeletedUsers from '@/components/Users/DeletedUsers'
import Roles from '@/components/Users/Roles'
import User from '@/components/Users/User'
import Users from '@/components/Users/Users'
import UserTypes from '@/components/Users/UserTypes'
import AddUser from '@/components/Users/AddUser'

// Defaults
import Defaults from '@/components/Defaults/Defaults'

// Zones
import AddZone from '@/components/NoGoZones/AddZone'
import DeletedZones from '@/components/NoGoZones/DeletedZones'

// Account Lines
import AccountLine from '@/components/AccountLines/AccountLine'
import AddAccountLine from '@/components/AccountLines/AddAccountLine'
import AccountLines from '@/components/AccountLines/AccountLines'
import DeletedAccountLines from '@/components/AccountLines/DeletedAccountLines'

// Reports
import CompanyReports from '@/components/Reports/CompanyReports'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/admin',
      name: 'Admin',
      component: Admin,
      children: [
        {
          path: 'dashboard',
          name: 'Admin.Dashboard',
          component: AdminDashboard
        },
        // Rides
        {
          path: 'trip/create',
          name: 'Admin.CreateTrip',
          component: CreateTrip
        },
        {
          path: 'rides/all',
          name: 'Admin.AllTrips',
          component: AllTrips
        },
        {
          path: 'rides/assigned',
          name: 'Admin.AssignedTrips',
          component: AssignedTrips
        },
        {
          path: 'rides/completed',
          name: 'Admin.CompletedTrips',
          component: CompletedTrips
        },
        {
          path: 'rides/unassigned',
          name: 'Admin.UnAssignedTrips',
          component: UnAssignedTrips
        },
        {
          path: 'rides/session',
          name: 'Admin.SessionTrips',
          component: TripsInSession
        },
        {
          path: 'rides/notes/:ride_id',
          name: 'Admin.RideNote',
          component: Notes
        },
        {
          path: 'rides/block-booking/:ride_id',
          name: 'Admin.BlockBooking',
          component: BlockBooking
        },
        {
          path: 'rides/edit/:ride_id',
          name: 'Admin.EditTrip',
          component: EditTrip
        },
        {
          path: 'schedule/:ride_id',
          name: 'Admin.AssignDriver',
          component: DriverSchedule
        },
        {
          path: 'schedules',
          name: 'Admin.FullSchedule',
          component: FullSchedule
        },
        // Drivers
        {
          path: 'drivers',
          name: 'Admin.Drivers',
          component: Drivers
        },
        {
          path: 'drivers/add',
          name: 'Admin.AddDriver',
          component: AddDriver
        },
        {
          path: 'driver/:driver_id',
          name: 'Admin.Driver',
          component: Driver
        },
        // Vehicle Owners
        {
          path: 'vehicle/:vehicle_id',
          name: 'Admin.Vehicle',
          component: Vehicle
        },
        {
          path: 'vehicles',
          name: 'Admin.Vehicles',
          component: Vehicles
        },
        {
          path: 'vehicles/add',
          name: 'Admin.AddVehicle',
          component: AddVehicle
        },
        {
          path: 'vehicles/assign-driver',
          name: 'Admin.Assignment',
          component: AssignVehicle
        },
        // Vehicle Owners
        {
          path: 'vehicles-owners',
          name: 'Admin.Owners',
          component: Owners
        },
        {
          path: 'vehicles-owners/add',
          name: 'Admin.AddOwner',
          component: AddOwner
        },
        {
          path: 'vehicles-owner/:owner_id',
          name: 'Admin.Owner',
          component: Owner
        },
        // Assignments
        {
          path: 'assignments',
          name: 'Admin.Assignments',
          component: Assignments
        },
        // Institutions
        {
          path: 'companies/add',
          name: 'Admin.AddCompany',
          component: AddCompany
        },
        {
          path: 'companies',
          name: 'Admin.Companies',
          component: Companies
        },
        {
          path: 'companies/contract/:company_id',
          name: 'Admin.AddContract',
          component: AddContract
        },
        {
          path: 'companies/edit/contract/:contract_id',
          name: 'Admin.EditContract',
          component: EditContract
        },
        {
          path: 'contracts',
          name: 'Admin.Contracts',
          component: Contracts
        },
        {
          path: 'company/:company_id',
          name: 'Admin.Company',
          component: Company
        },
        // Users
        {
          path: 'users/deleted',
          name: 'Admin.DeletedUsers',
          component: DeletedUsers
        },
        {
          path: 'users',
          name: 'Admin.Users',
          component: Users
        },
        {
          path: 'roles',
          name: 'Admin.Roles',
          component: Roles
        },
        {
          path: 'user/:user_id',
          name: 'Admin.User',
          component: User
        },
        {
          path: 'users/types',
          name: 'Admin.UserTypes',
          component: UserTypes
        },
        {
          path: 'users/add',
          name: 'Admin.AddUser',
          component: AddUser
        },
        // Defaults
        {
          path: 'defaults',
          name: 'Admin.Defaults',
          component: Defaults
        }
      ]
    },
    {
      path: '/company',
      name: 'Company',
      component: MCompany,
      children: [
        {
          path: 'dashboard',
          name: 'Company.Dashboard',
          component: CompanyDashboard
        },
        // Trips
        {
          path: 'trip/create',
          name: 'Company.CreateTrip',
          component: CreateTrip
        },
        {
          path: 'rides/all',
          name: 'Company.AllTrips',
          component: AllTrips
        },
        {
          path: 'rides/completed',
          name: 'Company.CompletedTrips',
          component: CompletedTrips
        },
        {
          path: 'rides/session',
          name: 'Company.SessionTrips',
          component: TripsInSession
        },
        {
          path: 'rides/notes/:ride_id',
          name: 'Company.RideNote',
          component: Notes
        },
        {
          path: 'rides/block-booking/:ride_id',
          name: 'Company.BlockBooking',
          component: BlockBooking
        },
        {
          path: 'rides/unassigned',
          name: 'Company.UnAssignedTrips',
          component: UnAssignedTrips
        },
        {
          path: 'rides/edit/:ride_id',
          name: 'Company.EditTrip',
          component: EditTrip
        },
        // Users
        {
          path: 'users/deleted',
          name: 'Company.DeletedUsers',
          component: DeletedUsers
        },
        {
          path: 'users',
          name: 'Company.Users',
          component: Users
        },
        {
          path: 'roles',
          name: 'Company.Roles',
          component: Roles
        },
        {
          path: 'user/:user_id',
          name: 'Company.User',
          component: User
        },
        {
          path: 'users/types',
          name: 'Company.UserTypes',
          component: UserTypes
        },
        {
          path: 'users/add',
          name: 'Company.AddUser',
          component: AddUser
        },
        // Zones
        {
          path: 'zones/add',
          name: 'Company.AddZone',
          component: AddZone
        },
        {
          path: 'zones/deleted',
          name: 'Company.DeletedZones',
          component: DeletedZones
        },
        // Account Lines
        {
          path: 'account-line/:account_line',
          name: 'Company.AccountLine',
          component: AccountLine
        },
        {
          path: 'account-lines/add',
          name: 'Company.AddAccountLine',
          component: AddAccountLine
        },
        {
          path: 'account-lines',
          name: 'Company.AccountLines',
          component: AccountLines
        },
        {
          path: 'account-lines/deleted',
          name: 'Company.DeletedAccountLines',
          component: DeletedAccountLines
        },
        {
          path: 'reports',
          name: 'Company.Reports',
          component: CompanyReports
        }
      ]
    }
  ],
  mode: 'history'
})
