import Vue from 'vue'
import router from '../../router/index'
import {clientId, clientSecret} from '../defaults/store'
import {authUrls} from '../defaults/.urls'
import VueNotifications from 'vue-notifications'

export default {
  do_login (context, loginData) {
    const postData = {
      username: loginData.email,
      password: loginData.password,
      grant_type: 'password',
      client_id: clientId,
      client_secret: clientSecret,
      scope: ''
    }
    Vue.http.post(authUrls.login, postData).then(function (response) {
      if (response.status !== 200) {
        alert('Wrong Credentials')
      } else {
        localStorage.setItem('access_token', response.data.access_token)
        context.dispatch('get_credentials')
        VueNotifications.success({message: 'Login Successful'})
      }
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  get_credentials (context) {
    Vue.http.get(authUrls.user_details).then(function (response) {
      if (response.status !== 200) {
        alert('Wrong Credentials')
      } else {
        console.log(response.data)
        context.commit('GET_STATE', response.data)
        context.dispatch('find_view')
        VueNotifications.success({message: 'Credentials Received'})
      }
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.error})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  find_view (context) {
    const userType = context.state.session.user_type_id
    localStorage.setItem('mnbvcxz', userType)
    localStorage.setItem('user_name', context.state.session.first_name + ' ' + context.state.session.last_name)
    localStorage.setItem('lkjhgfdsa', context.state.session.public_id)
    if (userType === 5) {
      router.push({
        name: 'Admin.Dashboard'
      })
      VueNotifications.success({message: 'Welcome ' + localStorage.getItem('user_name')})
    } else if (userType === 3 || userType === 6) {
      VueNotifications.success({message: 'Welcome ' + localStorage.getItem('user_name')})
      context.dispatch('get_user_login', localStorage.getItem('lkjhgfdsa'))
    } else {
      alert('Use Mobile App')
      context.dispatch('reset_storage')
    }
  },
  reset_storage (context) {
    localStorage.clear()
    router.push({
      name: 'Hello'
    })
  }
}
