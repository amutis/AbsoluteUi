import mutations from './mutations'
import actions from './actions'

const state = {
  all_vehicles: [],
  vehicles: [],
  vehicle: [],
  deleted_vehicles: []
}

export default {
  state, mutations, actions
}
