import Vue from 'vue'
import {vehicleUrls} from '../defaults/.urls'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_vehicles (context) {
    Vue.http.get(vehicleUrls.getAllVehicles).then(function (response) {
      context.commit('GET_ALL_VEHICLES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_vehicles (context) {
    Vue.http.get(vehicleUrls.getVehicles).then(function (response) {
      context.commit('GET_VEHICLES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_vehicles (context) {
    Vue.http.get(vehicleUrls.getDeletedVehicle).then(function (response) {
      context.commit('GET_DELETED_VEHICLES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_vehicle (context, vehicleId) {
    Vue.http.get(vehicleUrls.getVehicle + vehicleId).then(function (response) {
      context.commit('GET_VEHICLE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_vehicle (context, data) {
    context.errors = []
    const postData = {
      vehicle_type: data.vehicle_type,
      vehicle_owner: data.vehicle_owner,
      vehicle_plate: data.vehicle_plate,
      phone_number: data.phone_number
    }
    Vue.http.post(vehicleUrls.postVehicle, postData).then(function (response) {
      context.dispatch('get_all_vehicles')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_vehicle (context, data) {
    context.errors = []
    const postData = {
      vehicle_type: data.vehicle_type,
      vehicle_owner: data.vehicle_owner,
      vehicle_plate: data.vehicle_plate,
      phone_number: data.phone_number
    }
    Vue.http.post(vehicleUrls.editVehicle + data.public_id, postData).then(function (response) {
      context.dispatch('get_all_vehicles')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_vehicle (context, publicId) {
    Vue.http.get(vehicleUrls.deleteVehicle + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_VEHICLES', response.data)
      router.push({
        name: 'Module.DeletedVehicles'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreVehicle (context, publicId) {
    Vue.http.get(vehicleUrls.restoreVehicle + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_VEHICLES', response.data)
      router.push({
        name: 'Module.Vehicles'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
