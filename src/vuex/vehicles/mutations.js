export default {
  GET_VEHICLES (state, data) {
    state.vehicles = data.data
  },
  GET_ALL_VEHICLES (state, data) {
    state.all_vehicles = data.data
  },
  GET_VEHICLE (state, data) {
    state.vehicle = data.data
  },
  GET_DELETED_VEHICLES (state, data) {
    state.deleted_vehicles = data.data
  }
}
