import Vue from 'vue'
import {userUrls} from '../defaults/.urls'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_users (context) {
    Vue.http.get(userUrls.getAllUsers).then(function (response) {
      context.commit('GET_ALL_USERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_users (context, publicId) {
    Vue.http.get(userUrls.getCompanyUsers + publicId).then(function (response) {
      context.commit('GET_USERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_users (context) {
    Vue.http.get(userUrls.getDeletedUser).then(function (response) {
      context.commit('GET_DELETED_USERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_user_login (context, userId) {
    Vue.http.get(userUrls.getUser + userId).then(function (response) {
      localStorage.setItem('zxcvbnm', response.data.data.company.public_id)
      router.push({
        name: 'Company.Dashboard'
      })
      context.dispatch('loading_false')
    })
  },
  get_user (context, userId) {
    Vue.http.get(userUrls.getUser + userId).then(function (response) {
      context.commit('GET_USER', response.data)
      context.dispatch('loading_false')
    })
  },
  post_user (context, data) {
    context.errors = []
    const postData = {
      user_type: data.user_type,
      first_name: data.first_name,
      last_name: data.last_name,
      email: data.email,
      phone_number: data.phone_number,
      company: data.company
    }
    Vue.http.post(userUrls.postUser, postData).then(function (response) {
      context.dispatch('get_all_users')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      console.log(error)
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
    })
  },
  update_user (context, data) {
    context.errors = []
    const postData = {
      user_type: data.user_type,
      first_name: data.first_name,
      last_name: data.last_name,
      email: data.email,
      phone_number: data.phone_number,
      company: data.company
    }
    Vue.http.post(userUrls.editUser + data.public_id, postData).then(function (response) {
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_user (context, publicId) {
    Vue.http.get(userUrls.deleteUser + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_USERS', response.data)
      router.push({
        name: 'Module.DeletedUsers'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreUser (context, publicId) {
    Vue.http.get(userUrls.restoreUser + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_USERS', response.data)
      router.push({
        name: 'Module.Users'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
