export default {
  GET_USERS (state, data) {
    state.users = data.data
  },
  GET_ALL_USERS (state, data) {
    state.all_users = data.data
  },
  GET_USER (state, data) {
    state.user = data.data
  },
  GET_DELETED_USERS (state, data) {
    state.deleted_users = data.data
  }
}
