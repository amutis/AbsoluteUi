import Vue from 'vue'
import {companyUrls} from '../defaults/.urls'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_companies (context) {
    Vue.http.get(companyUrls.getAllCompanies).then(function (response) {
      context.commit('GET_ALL_COMPANIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_companies (context) {
    Vue.http.get(companyUrls.getCompanies).then(function (response) {
      context.commit('GET_COMPANIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_company_count_users (context, publicId) {
    Vue.http.get(companyUrls.getCompanyCountUsers + publicId).then(function (response) {
      context.commit('COUNT_COMPANY_USERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_company_users (context, publicId) {
    Vue.http.get(companyUrls.getCompanyUsers + publicId).then(function (response) {
      context.commit('GET_COMPANY_USERS', response.data)
      console.log(response.data.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_companies (context) {
    Vue.http.get(companyUrls.getDeletedCompany).then(function (response) {
      context.commit('GET_DELETED_COMPANIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_company (context, companyId) {
    Vue.http.get(companyUrls.getCompany + companyId).then(function (response) {
      context.commit('GET_COMPANY', response.data)
      context.dispatch('loading_false')
    })
  },
  post_company (context, data) {
    context.errors = []
    const postData = {
      institution_name: data.institution_name,
      email: data.email,
      country: data.country,
      telephone: data.telephone,
      town: data.town,
      street: data.street,
      building: data.building
    }
    Vue.http.post(companyUrls.postCompany, postData).then(function (response) {
      context.dispatch('get_companies')
      router.push({
        name: 'Admin.Companies'
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_company (context, data) {
    context.errors = []
    const postData = {
      institution_name: data.institution_name,
      email: data.email,
      country: data.country,
      telephone: data.telephone,
      town: data.town,
      street: data.street,
      building: data.building
    }
    Vue.http.post(companyUrls.editCompany + data.public_id, postData).then(function (response) {
      context.dispatch('get_companies')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_company (context, publicId) {
    Vue.http.get(companyUrls.deleteCompany + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_COMPANIES', response.data)
      router.push({
        name: 'Module.DeletedCompanies'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreCompany (context, publicId) {
    Vue.http.get(companyUrls.restoreCompany + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_COMPANIES', response.data)
      router.push({
        name: 'Module.Companies'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
