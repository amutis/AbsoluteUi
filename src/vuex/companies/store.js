import mutations from './mutations'
import actions from './actions'

const state = {
  all_companies: [],
  companies: [],
  company: [],
  deleted_companies: [],
  company_user_count: {},
  company_users: []
}

export default {
  state, mutations, actions
}
