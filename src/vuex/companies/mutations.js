export default {
  GET_COMPANIES (state, data) {
    state.companies = data.data
  },
  GET_ALL_COMPANIES (state, data) {
    state.all_companies = data.data
  },
  GET_COMPANY (state, data) {
    state.company = data.data
  },
  GET_DELETED_COMPANIES (state, data) {
    state.deleted_companies = data.data
  },
  COUNT_COMPANY_USERS (state, data) {
    state.company_user_count = data.data
  },
  GET_COMPANY_USERS (state, data) {
    state.company_users = data.data
  }
}
