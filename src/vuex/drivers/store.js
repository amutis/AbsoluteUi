import mutations from './mutations'
import actions from './actions'

const state = {
  all_drivers: [],
  drivers: [],
  driver: [],
  deleted_drivers: [],
  driver_users: []
}

export default {
  state, mutations, actions
}
