import Vue from 'vue'
import {driverUrls} from '../defaults/.urls'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_drivers (context) {
    Vue.http.get(driverUrls.getAllDrivers).then(function (response) {
      context.commit('GET_ALL_DRIVERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_drivers (context) {
    Vue.http.get(driverUrls.getDrivers).then(function (response) {
      context.commit('GET_DRIVERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_driver_users (context) {
    Vue.http.get(driverUrls.getDriverUsers).then(function (response) {
      context.commit('GET_DRIVER_USERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_drivers (context) {
    Vue.http.get(driverUrls.getDeletedDriver).then(function (response) {
      context.commit('GET_DELETED_DRIVERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_driver (context, driverId) {
    Vue.http.get(driverUrls.getDriver + driverId).then(function (response) {
      context.commit('GET_DRIVER', response.data)
      context.dispatch('loading_false')
    })
  },
  post_driver (context, data) {
    const postData = {
      user: data.user,
      license_number: data.license_number,
      id_number: data.id_number,
      residence: data.residence
    }
    Vue.http.post(driverUrls.postDriver, postData).then(function (response) {
      context.dispatch('get_all_drivers')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_driver (context, data) {
    const postData = {
      license_number: data.license_number,
      id_number: data.id_number,
      residence: data.residence
    }
    Vue.http.post(driverUrls.editDriver + data.public_id, postData).then(function (response) {
      context.dispatch('get_drivers')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_driver (context, publicId) {
    Vue.http.get(driverUrls.deleteDriver + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_DRIVERS', response.data)
      router.push({
        name: 'Module.DeletedDrivers'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreDriver (context, publicId) {
    Vue.http.get(driverUrls.restoreDriver + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_DRIVERS', response.data)
      router.push({
        name: 'Module.Drivers'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
