export default {
  GET_DRIVERS (state, data) {
    state.drivers = data.data
  },
  GET_ALL_DRIVERS (state, data) {
    state.all_drivers = data.data
  },
  GET_DRIVER (state, data) {
    state.driver = data.data
  },
  GET_DELETED_DRIVERS (state, data) {
    state.deleted_drivers = data.data
  },
  GET_DRIVER_USERS (state, data) {
    state.driver_users = data.data
  }
}
