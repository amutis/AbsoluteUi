import Vue from 'vue'
import {assignmentUrls} from '../defaults/.urls'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_assignments (context) {
    Vue.http.get(assignmentUrls.getAllAssignments).then(function (response) {
      context.commit('GET_ALL_ASSIGNMENTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_assignments (context, publicId) {
    Vue.http.get(assignmentUrls.getAssignments + publicId).then(function (response) {
      context.commit('GET_ASSIGNMENTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_vehicle_assignments (context, publicId) {
    Vue.http.get(assignmentUrls.getVehicleAssignment + publicId).then(function (response) {
      context.commit('GET_ASSIGNMENTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_un_assigned_vehicle (context) {
    Vue.http.get(assignmentUrls.getUnAssignedVehicles).then(function (response) {
      context.commit('GET_UNASSIGNED_VEHICLE', response.data)
      context.dispatch('loading_false')
    })
  },
  get_un_assigned_driver (context) {
    Vue.http.get(assignmentUrls.getUnAssignedDrivers).then(function (response) {
      context.commit('GET_UNASSIGNED_DRIVER', response.data)
      context.dispatch('loading_false')
    })
  },
  get_assignment (context, assignmentId) {
    Vue.http.get(assignmentUrls.getAssignment + assignmentId).then(function (response) {
      context.commit('GET_ASSIGNMENT', response.data)
      context.dispatch('loading_false')
    })
  },
  post_assignment (context, data) {
    const postData = {
      vehicle: data.vehicle_id,
      phone_number: data.phone_number,
      driver: data.driver_id
    }
    Vue.http.post(assignmentUrls.postAssignment, postData).then(function (response) {
      context.dispatch('get_un_assigned_vehicle')
      context.dispatch('get_un_assigned_driver')
      context.dispatch('loading_false')
      VueNotifications.success({message: response.data.message})
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  do_ride_assignment (context, data) {
    const postData = {
      ride_id: data.ride_id,
      driver_id: data.driver_id
    }
    Vue.http.post(assignmentUrls.postTripAssignment, postData).then(function (response) {
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
      VueNotifications.success({message: response.data.message})
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_assignment (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(assignmentUrls.postAssignment, postData).then(function (response) {
      context.dispatch('get_all_assignments')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_assignment (context, publicId) {
    Vue.http.get(assignmentUrls.deleteAssignment + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_ASSIGNMENTS', response.data)
      router.push({
        name: 'Module.DeletedAssignments'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreAssignment (context, publicId) {
    Vue.http.get(assignmentUrls.restoreAssignment + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_ASSIGNMENTS', response.data)
      router.push({
        name: 'Module.Assignments'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
