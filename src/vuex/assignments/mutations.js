export default {
  GET_ASSIGNMENTS (state, data) {
    state.assignments = data.data
  },
  GET_ALL_ASSIGNMENTS (state, data) {
    state.all_assignments = data.data
  },
  GET_ASSIGNMENT (state, data) {
    state.assignment = data.data
  },
  GET_DELETED_ASSIGNMENTS (state, data) {
    state.deleted_assignments = data.data
  },
  GET_UNASSIGNED_VEHICLE (state, data) {
    state.unassigned_vehicles = data.data
  },
  GET_UNASSIGNED_DRIVER (state, data) {
    state.unassigned_drivers = data.data
  }
}
