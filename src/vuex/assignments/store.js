import mutations from './mutations'
import actions from './actions'

const state = {
  all_assignments: [],
  assignments: [],
  assignment: [],
  deleted_assignments: [],
  unassigned_vehicles: [],
  unassigned_drivers: []
}

export default {
  state, mutations, actions
}
