import mutations from './mutations'
import actions from './actions'

const state = {
  all_rides: [],
  rides: [],
  ride: [],
  deleted_rides: [],
  all_unassigned_trips: [],
  all_assigned_trips: [],
  all_in_session_trips: [],
  all_completed_trips: []
}

export default {
  state, mutations, actions
}
