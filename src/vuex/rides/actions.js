import Vue from 'vue'
import {rideUrls} from '../defaults/.urls'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_rides (context) {
    Vue.http.get(rideUrls.getAllRides).then(function (response) {
      context.commit('GET_ALL_RIDES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_all_unassigned_trips (context) {
    Vue.http.get(rideUrls.getUnassignedTrips).then(function (response) {
      context.commit('GET_ALL_UNASSIGNED_RIDES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_driver_rides (context, data) {
    Vue.http.get(rideUrls.getDriverTrips + data.public_id + '/' + data.ride_id).then(function (response) {
      context.commit('GET_RIDES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_all_assigned_trips (context) {
    Vue.http.get(rideUrls.getAssignedTrips).then(function (response) {
      context.commit('GET_ALL_ASSIGNED_RIDES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_all_in_session_trips (context) {
    Vue.http.get(rideUrls.getInSessionTrips).then(function (response) {
      context.commit('GET_ALL_IN_SESSION_RIDES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_all_completed_trips (context) {
    Vue.http.get(rideUrls.getCompletedTrips).then(function (response) {
      context.commit('GET_ALL_COMPLETED_RIDES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_rides (context, publicId) {
    Vue.http.get(rideUrls.getRides + publicId).then(function (response) {
      context.commit('GET_RIDES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_user_rides (context, publicId) {
    Vue.http.get(rideUrls.getUserRide + publicId).then(function (response) {
      context.commit('GET_RIDES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_rides (context) {
    Vue.http.get(rideUrls.getDeletedRide).then(function (response) {
      context.commit('GET_DELETED_RIDES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_ride (context, rideId) {
    Vue.http.get(rideUrls.getRide + rideId).then(function (response) {
      context.commit('GET_RIDE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_note (context, data) {
    const postData = {
      note: data.written
    }
    Vue.http.post(rideUrls.postNote + data.public_id, postData).then(function (response) {
      context.dispatch('get_all_starts')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
     // console.log(error.data)
    })
  },
  upload_file_note (context, data) {
    const postData = {
      file_note: data.file_note
    }
    Vue.http.post(rideUrls.uploadNote + data.public_id, postData).then(function (response) {
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      // console.log(error.data)
      context.dispatch('loading_false')
    })
  },
  block_book (context, data) {
    const postData = {
      final_date: data.date,
      repeat: data.type
    }
    Vue.http.post(rideUrls.postBlockBook + data.public_id, postData).then(function () {
      context.dispatch('get_all_starts')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  post_ride (context, data) {
    const postData = {
      company: data.company,
      passenger: data.user,
      ride_type: data.ride_type,
      vehicle_type: data.vehicle_type,
      account_line: data.account_line,
      origin: data.origin,
      destination: data.destination,
      ride_date: data.b_date,
      ride_time: data.b_time
    }
    Vue.http.post(rideUrls.postRide, postData).then(function () {
      context.dispatch('get_all_rides')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_ride (context, data) {
    const postData = {
      company: data.company,
      passenger: data.user,
      ride_type: data.ride_type,
      vehicle_type: data.vehicle_type,
      account_line: data.account_line,
      origin: data.origin,
      destination: data.destination,
      ride_date: data.b_date,
      ride_time: data.b_time
    }
    Vue.http.post(rideUrls.editRide + data.public_id, postData).then(function () {
      context.dispatch('get_all_rides')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_ride (context, publicId) {
    Vue.http.get(rideUrls.deleteRide + publicId).then(function (response) {
      context.dispatch('get_all_unassigned_trips')
      context.dispatch('get_all_assigned_trips')
      context.dispatch('get_all_in_session_trips')
      context.dispatch('get_all_completed_trips')
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
      // alert(response.data.message)
    })
  },
  restoreRide (context, publicId) {
    Vue.http.get(rideUrls.restoreRide + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_RIDES', response.data)
      router.push({
        name: 'Module.Rides'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
