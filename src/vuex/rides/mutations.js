export default {
  GET_RIDES (state, data) {
    state.rides = data.data
  },
  GET_ALL_RIDES (state, data) {
    state.all_rides = data.data
  },
  GET_RIDE (state, data) {
    state.ride = data.data
  },
  GET_DELETED_RIDES (state, data) {
    state.deleted_rides = data.data
  },
  GET_ALL_UNASSIGNED_RIDES (state, data) {
    state.all_unassigned_trips = data.data
  },
  GET_ALL_ASSIGNED_RIDES (state, data) {
    state.all_assigned_trips = data.data
  },
  GET_ALL_IN_SESSION_RIDES (state, data) {
    state.all_in_session_trips = data.data
  },
  GET_ALL_COMPLETED_RIDES (state, data) {
    state.all_completed_trips = data.data
  }
}
