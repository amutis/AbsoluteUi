export default {
  GET_ACCOUNT_LINES (state, data) {
    state.acount_lines = data.data
  },
  GET_ALL_ACCOUNT_LINES (state, data) {
    state.all_acount_lines = data.data
  },
  GET_ACCOUNT_LINE (state, data) {
    state.acount_line = data.data
  },
  GET_DELETED_ACCOUNT_LINES (state, data) {
    state.deleted_acount_lines = data.data
  }
}
