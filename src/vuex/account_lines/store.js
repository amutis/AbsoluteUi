import mutations from './mutations'
import actions from './actions'

const state = {
  all_acount_lines: [],
  acount_lines: [],
  acount_line: [],
  deleted_acount_lines: []
}

export default {
  state, mutations, actions
}
