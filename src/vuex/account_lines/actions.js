import Vue from 'vue'
import {accountLineUrls} from '../defaults/.urls'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_account_lines (context) {
    Vue.http.get(accountLineUrls.getAllAccountLines).then(function (response) {
      context.commit('GET_ALL_ACCOUNT_LINES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_account_lines (context, publicId) {
    Vue.http.get(accountLineUrls.getAccountLines + publicId).then(function (response) {
      context.commit('GET_ACCOUNT_LINES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_account_lines (context) {
    Vue.http.get(accountLineUrls.getDeletedAccountLine).then(function (response) {
      context.commit('GET_DELETED_ACCOUNT_LINES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_account_line (context, publicId) {
    Vue.http.get(accountLineUrls.getAccountLine + publicId).then(function (response) {
      context.commit('GET_ACCOUNT_LINE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_account_line (context, data) {
    context.errors = []
    const postData = {
      account_line_name: data.account_line_name,
      description: data.description,
      company_id: data.company_id
    }
    Vue.http.post(accountLineUrls.postAccountLine, postData).then(function (response) {
      context.dispatch('get_all_account_lines')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_account_line (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(accountLineUrls.postAccountLine, postData).then(function (response) {
      context.dispatch('get_account_lines')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_account_line (context, data) {
    Vue.http.get(accountLineUrls.deleteAccountLine + data.public_id).then(function (response) {
      alert(response.data.message)
      context.dispatch('get_account_lines', data.company_id)
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreAccountLine (context, publicId) {
    Vue.http.get(accountLineUrls.restoreAccountLine + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_ACCOUNT_LINES', response.data)
      router.push({
        name: 'Module.AccountLines'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
