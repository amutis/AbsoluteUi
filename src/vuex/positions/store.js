import mutations from './mutations'
import actions from './actions'

const state = {
  all_positions: [],
  positions: [],
  position: [],
  deleted_positions: []
}

export default {
  state, mutations, actions
}
