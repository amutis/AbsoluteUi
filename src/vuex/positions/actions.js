import Vue from 'vue'
import {positionUrls} from '../defaults/.urls'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_positions (context) {
    Vue.http.get(positionUrls.getAllPositions).then(function (response) {
      context.commit('GET_ALL_POSITIONS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_positions (context) {
    Vue.http.get(positionUrls.getPositions).then(function (response) {
      context.commit('GET_POSITIONS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_positions (context) {
    Vue.http.get(positionUrls.getDeletedPosition).then(function (response) {
      context.commit('GET_DELETED_POSITIONS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_position (context, positionId) {
    Vue.http.get(positionUrls.getPosition + positionId).then(function (response) {
      context.commit('GET_POSITION', response.data)
      context.dispatch('loading_false')
    })
  },
  get_vehicle_position (context, positionId) {
    Vue.http.get(positionUrls.getVehiclePosition + positionId).then(function (response) {
      context.commit('GET_POSITION', response.data)
      context.dispatch('loading_false')
    })
  },
  post_position (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(positionUrls.postPosition, postData).then(function (response) {
      context.dispatch('get_all_positions')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_position (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(positionUrls.postPosition, postData).then(function (response) {
      context.dispatch('get_all_positions')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_position (context, publicId) {
    Vue.http.get(positionUrls.deletePosition + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_POSITIONS', response.data)
      router.push({
        name: 'Module.DeletedPositions'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restorePosition (context, publicId) {
    Vue.http.get(positionUrls.restorePosition + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_POSITIONS', response.data)
      router.push({
        name: 'Module.Positions'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
