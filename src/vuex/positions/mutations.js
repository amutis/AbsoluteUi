export default {
  GET_POSITIONS (state, data) {
    state.positions = data.data
  },
  GET_ALL_POSITIONS (state, data) {
    state.all_positions = data.data
  },
  GET_POSITION (state, data) {
    state.position = data.data
  },
  GET_DELETED_POSITIONS (state, data) {
    state.deleted_positions = data.data
  }
}
