export default {
  UPDATE_ERRORS (state, errors) {
    state.all_errors = errors.errors
  },
  CLEAR_ERRORS (state) {
    state.all_errors = []
  }
}
