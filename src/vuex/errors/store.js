import mutations from './mutations'
import actions from './actions'

const state = {
  all_errors: []
}

export default {
  state, mutations, actions
}
