export default {
  GET_ZONES (state, data) {
    state.zones = data.data
  },
  GET_ALL_ZONES (state, data) {
    state.all_zones = data.data
  },
  GET_ZONE (state, data) {
    state.zone = data.data
  },
  GET_DELETED_ZONES (state, data) {
    state.deleted_zones = data.data
  }
}
