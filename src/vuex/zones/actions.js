import Vue from 'vue'
import {zoneUrls} from '../defaults/.urls'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_zones (context) {
    Vue.http.get(zoneUrls.getAllZones).then(function (response) {
      context.commit('GET_ALL_ZONES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_zones (context, publicId) {
    Vue.http.get(zoneUrls.getZones + publicId).then(function (response) {
      context.commit('GET_ZONES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_company_zones (context, publicId) {
    Vue.http.get(zoneUrls.getCompanyZones + publicId).then(function (response) {
      context.commit('GET_ZONES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_zones (context) {
    Vue.http.get(zoneUrls.getDeletedZone).then(function (response) {
      context.commit('GET_DELETED_ZONES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_zone (context, zoneId) {
    Vue.http.get(zoneUrls.getZone + zoneId).then(function (response) {
      context.commit('GET_ZONE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_zone (context, data) {
    context.errors = []
    const postData = {
      zone_name: data.zone_name,
      description: data.description,
      company_id: data.company_id
    }
    Vue.http.post(zoneUrls.postZone, postData).then(function (response) {
      context.dispatch('get_company_zones', data.company_id)
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_zone (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(zoneUrls.postZone, postData).then(function (response) {
      context.dispatch('get_all_zones')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_zone (context, data) {
    Vue.http.get(zoneUrls.deleteZone + data.public_id).then(function (response) {
      alert(response.data.message)
      context.dispatch('get_company_zones', data.company_id)
      context.dispatch('loading_false')
      VueNotifications.info({message: response.data.message})
    })
  },
  restoreZone (context, publicId) {
    Vue.http.get(zoneUrls.restoreZone + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_ZONES', response.data)
      router.push({
        name: 'Module.Zones'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
