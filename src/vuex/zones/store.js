import mutations from './mutations'
import actions from './actions'

const state = {
  all_zones: [],
  zones: [],
  zone: [],
  deleted_zones: []
}

export default {
  state, mutations, actions
}
