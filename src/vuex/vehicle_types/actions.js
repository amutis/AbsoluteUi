import Vue from 'vue'
import {vehicleTypeUrls} from '../defaults/.urls'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_vehicle_types (context) {
    Vue.http.get(vehicleTypeUrls.getAllVehicleTypes).then(function (response) {
      context.commit('GET_ALL_STARTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_vehicle_types (context) {
    Vue.http.get(vehicleTypeUrls.getVehicleTypes).then(function (response) {
      context.commit('GET_STARTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_vehicle_types (context) {
    Vue.http.get(vehicleTypeUrls.getDeletedVehicleType).then(function (response) {
      context.commit('GET_DELETED_STARTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_vehicle_type (context, publicId) {
    Vue.http.get(vehicleTypeUrls.getVehicleType + publicId).then(function (response) {
      context.commit('GET_START', response.data)
      context.dispatch('loading_false')
    })
  },
  post_vehicle_type (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(vehicleTypeUrls.postVehicleType, postData).then(function (response) {
      context.dispatch('get_all_vehicle_types')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_vehicle_type (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(vehicleTypeUrls.postVehicleType, postData).then(function (response) {
      context.dispatch('get_all_vehicle_types')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_vehicle_type (context, publicId) {
    Vue.http.get(vehicleTypeUrls.deleteVehicleType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_STARTS', response.data)
      router.push({
        name: 'Module.DeletedVehicleTypes'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreVehicleType (context, publicId) {
    Vue.http.get(vehicleTypeUrls.restoreVehicleType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_STARTS', response.data)
      router.push({
        name: 'Module.VehicleTypes'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
