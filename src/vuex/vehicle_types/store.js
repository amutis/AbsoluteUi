import mutations from './mutations'
import actions from './actions'

const state = {
  all_vehicle_types: [],
  vehicle_types: [],
  vehicle_type: [],
  deleted_vehicle_types: []
}

export default {
  state, mutations, actions
}
