export default {
  GET_STARTS (state, data) {
    state.vehicle_types = data.data
  },
  GET_ALL_STARTS (state, data) {
    state.all_vehicle_types = data.data
  },
  GET_START (state, data) {
    state.vehicle_type = data.data
  },
  GET_DELETED_STARTS (state, data) {
    state.deleted_vehicle_types = data.data
  }
}
