import Vue from 'vue'
import {countryUrls} from '../defaults/.urls'
import router from '../../router/index'

export default {
  get_all_countries (context) {
    Vue.http.get(countryUrls.getAllCountries).then(function (response) {
      context.commit('GET_ALL_COUNTRIES', response.data)
    })
  },
  get_countries (context) {
    Vue.http.get(countryUrls.getCountries).then(function (response) {
      context.commit('GET_COUNTRIES', response.data)
    })
  },
  get_deleted_countries (context) {
    Vue.http.get(countryUrls.getDeletedCountry).then(function (response) {
      context.commit('GET_DELETED_COUNTRIES', response.data)
    })
  },
  get_country (context, countryId) {
    Vue.http.get(countryUrls.getCountry + countryId).then(function (response) {
      context.commit('GET_COUNTRY', response.data)
    })
  },
  post_country (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(countryUrls.postCountry, postData).then(function () {
      context.dispatch('get_all_countries')
      router.push({
        name: data.redirect_url
      })
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      // console.log(error.data)
    })
  },
  update_country (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(countryUrls.postCountry, postData).then(function () {
      context.dispatch('get_all_countries')
      router.push({
        name: data.redirect_url
      })
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      // console.log(error.data)
    })
  },
  delete_country (context, publicId) {
    Vue.http.get(countryUrls.deleteCountry + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_COUNTRIES', response.data)
      router.push({
        name: 'Module.DeletedCountries'
      })
    })
  },
  restoreCountry (context, publicId) {
    Vue.http.get(countryUrls.restoreCountry + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_COUNTRIES', response.data)
      router.push({
        name: 'Module.Countries'
      })
    })
  }
}
