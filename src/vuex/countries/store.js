import mutations from './mutations'
import actions from './actions'

const state = {
  all_countries: [],
  countries: [],
  country: [],
  deleted_countries: []
}

export default {
  state, mutations, actions
}
