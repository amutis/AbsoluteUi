// export const mainUrl = 'http://127.0.0.1:8000/'
export const mainUrl = 'http://absoluteback.tymizawebapps.com/'

export const authUrls = {
  login: mainUrl + 'oauth/token',
  user_details: mainUrl + 'vue/user'
}

export const userTypeUrls = {
  postUserType: mainUrl + 'vue/user_types/post',
  getUserTypes: mainUrl + 'vue/user_types',
  getUserType: mainUrl + 'vue/user_types/single/',
  deleteUserType: mainUrl + 'vue/user_types/delete/',
  editUserType: mainUrl + 'vue/user_types/edit/',
  getAllUserTypes: mainUrl + 'vue/user_types/all',
  restoreUserType: mainUrl + 'vue/user_types/restore/',
  getDeletedUserType: mainUrl + 'vue/user_types/deleted'
}

export const companyUrls = {
  postCompany: mainUrl + 'vue/companies/post',
  getCompanies: mainUrl + 'vue/companies',
  getCompany: mainUrl + 'vue/companies/single/',
  deleteCompany: mainUrl + 'vue/companies/delete/',
  editCompany: mainUrl + 'vue/companies/edit/',
  getAllCompanies: mainUrl + 'vue/companies/all',
  restoreCompany: mainUrl + 'vue/companies/restore/',
  getDeletedCompany: mainUrl + 'vue/companies/deleted',

  getCompanyCountUsers: mainUrl + 'vue/users/company/count/',
  getCompanyUsers: mainUrl + 'vue/users/company/'
}

export const countryUrls = {
  postCountry: mainUrl + 'vue/countries/post',
  getCountries: mainUrl + 'vue/countries',
  getCountry: mainUrl + 'vue/countries/single/',
  deleteCountry: mainUrl + 'vue/countries/delete/',
  editCountry: mainUrl + 'vue/countries/edit/',
  getAllCountries: mainUrl + 'vue/countries/all',
  restoreCountry: mainUrl + 'vue/countries/restore/',
  getDeletedCountry: mainUrl + 'vue/countries/deleted'
}

export const userUrls = {
  postUser: mainUrl + 'vue/users/post',
  getUsers: mainUrl + 'vue/users',
  getAllUsers: mainUrl + 'vue/users/all',
  getUser: mainUrl + 'vue/users/single/',
  deleteUser: mainUrl + 'vue/users/delete/',
  editUser: mainUrl + 'vue/users/edit/',
  getCompanyUsers: mainUrl + 'vue/users/company/',
  restoreUser: mainUrl + 'vue/users/restore/',
  getDeletedUser: mainUrl + 'vue/users/deleted',
  getUserOutlet: mainUrl + 'vue/user'
}

export const rideUrls = {
  postRide: mainUrl + 'vue/rides/post',
  getRides: mainUrl + 'vue/rides',
  getUserRides: mainUrl + 'vue/rides/user/',
  getRide: mainUrl + 'vue/rides/single/',
  deleteRide: mainUrl + 'vue/rides/delete/',
  editRide: mainUrl + 'vue/rides/edit/',
  getAllRides: mainUrl + 'vue/rides',
  restoreRide: mainUrl + 'vue/rides/restore/',
  getDeletedRide: mainUrl + 'vue/rides/deleted',

  getUserRide: mainUrl + 'vue/rides/user/',
  getDriverTrips: mainUrl + 'vue/rides/driver/',
  getUnassignedTrips: mainUrl + 'vue/rides/unassigned',
  getInSessionTrips: mainUrl + 'vue/rides/in_session',
  getCompletedTrips: mainUrl + 'vue/rides/completed',
  getAssignedTrips: mainUrl + 'vue/rides/assigned',
  postNote: mainUrl + 'vue/rides/note/',
  uploadNote: mainUrl + 'vue/rides/upload/',
  postBlockBook: mainUrl + 'vue/rides/block/'

}

export const rideTypeUrls = {
  postRideType: mainUrl + 'vue/ride_types/post',
  getRideTypes: mainUrl + 'vue/ride_types',
  getRideType: mainUrl + 'vue/ride_types/single/',
  deleteRideType: mainUrl + 'vue/ride_types/delete/',
  editRideType: mainUrl + 'vue/ride_types/edit/',
  getAllRideTypes: mainUrl + 'vue/ride_types',
  restoreRideType: mainUrl + 'vue/ride_types/restore/',
  getDeletedRideType: mainUrl + 'vue/ride_types/deleted'
}

export const accountLineUrls = {
  postAccountLine: mainUrl + 'vue/account_lines/post',
  getAccountLines: mainUrl + 'vue/account_lines/company/',
  getAccountLine: mainUrl + 'vue/account_lines/single/',
  deleteAccountLine: mainUrl + 'vue/account_lines/delete/',
  editAccountLine: mainUrl + 'vue/account_lines/edit/',
  getAllAccountLines: mainUrl + 'vue/account_lines/all',
  restoreAccountLine: mainUrl + 'vue/account_lines/restore/',
  getDeletedAccountLine: mainUrl + 'vue/account_lines/deleted'
}

export const vehicleTypeUrls = {
  postVehicleType: mainUrl + 'vue/vehicle_types/post',
  getVehicleTypes: mainUrl + 'vue/vehicle_types',
  getVehicleType: mainUrl + 'vue/vehicle_types/single/',
  deleteVehicleType: mainUrl + 'vue/vehicle_types/delete/',
  editVehicleType: mainUrl + 'vue/vehicle_types/edit/',
  getAllVehicleTypes: mainUrl + 'vue/vehicle_types',
  restoreVehicleType: mainUrl + 'vue/vehicle_types/restore/',
  getDeletedVehicleType: mainUrl + 'vue/vehicle_types/deleted'
}

export const driverUrls = {
  postDriver: mainUrl + 'vue/drivers/post',
  getDrivers: mainUrl + 'vue/drivers',
  getDriverUsers: mainUrl + 'vue/drivers/users',
  getDriver: mainUrl + 'vue/drivers/single/',
  deleteDriver: mainUrl + 'vue/drivers/delete/',
  editDriver: mainUrl + 'vue/drivers/edit/',
  getAllDrivers: mainUrl + 'vue/drivers/all',
  restoreDriver: mainUrl + 'vue/drivers/restore/',
  getDeletedDriver: mainUrl + 'vue/drivers/deleted'
}

export const positionUrls = {
  postPosition: mainUrl + 'vue/positions/post',
  getPositions: mainUrl + 'vue/positions',
  getPosition: mainUrl + 'vue/positions/single/',
  deletePosition: mainUrl + 'vue/positions/delete/',
  editPosition: mainUrl + 'vue/positions/edit/',
  getAllPositions: mainUrl + 'vue/positions/all',
  restorePosition: mainUrl + 'vue/positions/restore/',
  getDeletedPosition: mainUrl + 'vue/positions/deleted',

  getVehiclePosition: mainUrl + 'vue/positions/vehicle/'
}

export const vehicleUrls = {
  postVehicle: mainUrl + 'vue/vehicles/post',
  getVehicles: mainUrl + 'vue/vehicles',
  getVehicle: mainUrl + 'vue/vehicles/single/',
  deleteVehicle: mainUrl + 'vue/vehicles/delete/',
  editVehicle: mainUrl + 'vue/vehicles/edit/',
  getAllVehicles: mainUrl + 'vue/vehicles/all',
  restoreVehicle: mainUrl + 'vue/vehicles/restore/',
  getDeletedVehicle: mainUrl + 'vue/vehicles/deleted'
}

export const vehicleOwnerUrls = {
  postVehicleOwner: mainUrl + 'vue/vehicle_owners/post',
  getVehicleOwners: mainUrl + 'vue/vehicle_owners',
  getVehicleOwner: mainUrl + 'vue/vehicle_owners/single/',
  deleteVehicleOwner: mainUrl + 'vue/vehicle_owners/delete/',
  editVehicleOwner: mainUrl + 'vue/vehicle_owners/edit/',
  getAllVehicleOwners: mainUrl + 'vue/vehicle_owners/all',
  restoreVehicleOwner: mainUrl + 'vue/vehicle_owners/restore/',
  getDeletedVehicleOwner: mainUrl + 'vue/vehicle_owners/deleted'
}

export const assignmentUrls = {
  postAssignment: mainUrl + 'vue/assignments/post',
  postTripAssignment: mainUrl + 'vue/assignments/ride',
  getAssignments: mainUrl + 'vue/assignments',
  getAssignment: mainUrl + 'vue/assignments/single/',
  deleteAssignment: mainUrl + 'vue/assignments/delete/',
  editAssignment: mainUrl + 'vue/assignments/edit/',
  getAllAssignments: mainUrl + 'vue/assignments/all',
  restoreAssignment: mainUrl + 'vue/assignments/restore/',
  getDeletedAssignment: mainUrl + 'vue/assignments/deleted',

  getVehicleAssignment: mainUrl + 'vue/assignments/vehicle/',
  getUnAssignedDrivers: mainUrl + 'vue/assignments/unassigned/drivers',
  getUnAssignedVehicles: mainUrl + 'vue/assignments/unassigned/vehicles'
}

export const contractUrls = {
  postContract: mainUrl + 'vue/contracts/post/',
  getContracts: mainUrl + 'vue/contracts',
  getContract: mainUrl + 'vue/contracts/single/',
  getCompanyContract: mainUrl + 'vue/contracts/company/',
  deleteContract: mainUrl + 'vue/contracts/delete/',
  editContract: mainUrl + 'vue/contracts/edit/',
  getAllContracts: mainUrl + 'vue/contracts/all',
  restoreContract: mainUrl + 'vue/contracts/restore/',
  getDeletedContract: mainUrl + 'vue/contracts/deleted'
}

export const zoneUrls = {
  postZone: mainUrl + 'vue/zones/post',
  getZones: mainUrl + 'vue/zones',
  getZone: mainUrl + 'vue/zones/single/',
  getCompanyZones: mainUrl + 'vue/zones/company/',
  deleteZone: mainUrl + 'vue/zones/delete/',
  editZone: mainUrl + 'vue/zones/edit/',
  getAllZones: mainUrl + 'vue/zones/all',
  restoreZone: mainUrl + 'vue/zones/restore/',
  getDeletedZone: mainUrl + 'vue/zones/deleted'
}
