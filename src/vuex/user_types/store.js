import mutations from './mutations'
import actions from './actions'

const state = {
  user_types: []
}

export default {
  state, mutations, actions
}
