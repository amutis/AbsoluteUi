import Vue from 'vue'
import {userTypeUrls} from '../defaults/.urls'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_user_types (context) {
    Vue.http.get(userTypeUrls.getAllUserTypes).then(function (response) {
      context.commit('GET_ALL_USER_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_user_types (context) {
    Vue.http.get(userTypeUrls.getUserTypes).then(function (response) {
      context.commit('GET_USER_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_user_types (context) {
    Vue.http.get(userTypeUrls.getDeletedUserType).then(function (response) {
      context.commit('GET_DELETED_USER_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_user_type (context, publicId) {
    Vue.http.get(userTypeUrls.getUserType + publicId).then(function (response) {
      context.commit('GET_USER_TYPE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_user_type (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name,
      last_name: data.last_name,
      email: data.email,
      user_type_type: data.user_type_type,
      phone_number: data.phone_number
    }
    Vue.http.post(userTypeUrls.postUserType, postData).then(function (response) {
      context.dispatch('get_all_user_types')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_user_type (context, publicId) {
    Vue.http.get(userTypeUrls.deleteUserType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_USER_TYPES', response.data)
      router.push({
        name: 'Admin.DeletedUserTypes'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreuserType (context, publicId) {
    Vue.http.get(userTypeUrls.restoreUserType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_USER_TYPES', response.data)
      router.push({
        name: 'Admin.userTypes'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
