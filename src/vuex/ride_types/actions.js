import Vue from 'vue'
import {rideTypeUrls} from '../defaults/.urls'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_ride_types (context) {
    Vue.http.get(rideTypeUrls.getAllRideTypes).then(function (response) {
      context.commit('GET_ALL_RIDE_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_ride_type (context, publicId) {
    Vue.http.get(rideTypeUrls.getRideTypes + publicId).then(function (response) {
      context.commit('GET_RIDE_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_ride_type (context) {
    Vue.http.get(rideTypeUrls.getDeletedRideType).then(function (response) {
      context.commit('GET_DELETED_RIDE_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  post_ride_type (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(rideTypeUrls.postRideType, postData).then(function (response) {
      context.dispatch('get_all_ride_type')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_ride_type (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(rideTypeUrls.postRideType, postData).then(function (response) {
      context.dispatch('get_all_ride_type')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_ride_type (context, publicId) {
    Vue.http.get(rideTypeUrls.deleteRideType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_RIDE_TYPES', response.data)
      router.push({
        name: 'Module.DeletedRideTypes'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreRideType (context, publicId) {
    Vue.http.get(rideTypeUrls.restoreRideType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_RIDE_TYPES', response.data)
      router.push({
        name: 'Module.RideTypes'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
