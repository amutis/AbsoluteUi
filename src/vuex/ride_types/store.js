import mutations from './mutations'
import actions from './actions'

const state = {
  all_ride_types: [],
  ride_types: [],
  ride_type: [],
  deleted_ride_type: []
}

export default {
  state, mutations, actions
}
