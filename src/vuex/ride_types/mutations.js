export default {
  GET_RIDE_TYPES (state, data) {
    state.ride_types = data.data
  },
  GET_ALL_RIDE_TYPES (state, data) {
    state.all_ride_types = data.data
  },
  GET_RIDE_TYPE (state, data) {
    state.start = data.data
  },
  GET_DELETED_RIDE_TYPES (state, data) {
    state.deleted_ride_type = data.data
  }
}
