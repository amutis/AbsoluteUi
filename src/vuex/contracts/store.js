import mutations from './mutations'
import actions from './actions'

const state = {
  all_contracts: [],
  contracts: [],
  contract: [],
  deleted_contracts: []
}

export default {
  state, mutations, actions
}
