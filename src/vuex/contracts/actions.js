import Vue from 'vue'
import {contractUrls} from '../defaults/.urls'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_contracts (context) {
    Vue.http.get(contractUrls.getAllContracts).then(function (response) {
      context.commit('GET_ALL_CONTRACTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_contracts (context, publicId) {
    Vue.http.get(contractUrls.getContracts + publicId).then(function (response) {
      context.commit('GET_CONTRACTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_contracts (context) {
    Vue.http.get(contractUrls.getDeletedContract).then(function (response) {
      context.commit('GET_DELETED_CONTRACTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_contract (context, contractId) {
    Vue.http.get(contractUrls.getContract + contractId).then(function (response) {
      context.commit('GET_CONTRACT', response.data)
      context.dispatch('loading_false')
    })
  },
  get_company_contract (context, contractId) {
    Vue.http.get(contractUrls.getCompanyContract + contractId).then(function (response) {
      context.commit('GET_CONTRACTS', response.data)
      context.dispatch('loading_false')
    })
  },
  post_contract (context, data) {
    const postData = {
      contract_name: data.contract_name,
      minimum_price: data.minimum_price,
      price_per_km: data.price_per_km,
      price_per_min: data.price_per_min,
      expiry_date: data.expiry_date
    }
    Vue.http.post(contractUrls.postContract + data.public_id, postData).then(function (response) {
      context.dispatch('get_company_contract', data.public_id)
      router.push({
        name: data.redirect_url,
        params: {
          company_id: data.public_id
        }
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_contract (context, data) {
    const postData = {
      contract_name: data.contract_name,
      minimum_price: data.minimum_price,
      price_per_km: data.price_per_km,
      price_per_min: data.price_per_min,
      expiry_date: data.expiry_date
    }
    Vue.http.post(contractUrls.editContract + data.public_id, postData).then(function (response) {
      router.push({
        name: 'Admin.Companies'
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_contract (context, publicId) {
    Vue.http.get(contractUrls.deleteContract + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_CONTRACTS', response.data)
      router.push({
        name: 'Module.DeletedContracts'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreContract (context, publicId) {
    Vue.http.get(contractUrls.restoreContract + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_CONTRACTS', response.data)
      router.push({
        name: 'Module.Contracts'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
