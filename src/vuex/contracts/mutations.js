export default {
  GET_CONTRACTS (state, data) {
    state.contracts = data.data
  },
  GET_ALL_CONTRACTS (state, data) {
    state.all_contracts = data.data
  },
  GET_CONTRACT (state, data) {
    state.contract = data.data
  },
  GET_DELETED_CONTRACTS (state, data) {
    state.deleted_contracts = data.data
  }
}
