import Vue from 'vue'
import {vehicleOwnerUrls} from '../defaults/.urls'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_vehicle_owners (context) {
    Vue.http.get(vehicleOwnerUrls.getAllVehicleOwners).then(function (response) {
      context.commit('GET_ALL_VEHICLE_OWNERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_vehicle_owners (context) {
    Vue.http.get(vehicleOwnerUrls.getVehicleOwners).then(function (response) {
      context.commit('GET_VEHICLE_OWNERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_vehicle_owners (context) {
    Vue.http.get(vehicleOwnerUrls.getDeletedVehicleOwner).then(function (response) {
      context.commit('GET_DELETED_VEHICLE_OWNERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_vehicle_owner (context, publicId) {
    Vue.http.get(vehicleOwnerUrls.getVehicleOwner + publicId).then(function (response) {
      context.commit('GET_VEHICLE_OWNER', response.data)
      context.dispatch('loading_false')
    })
  },
  post_vehicle_owner (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name,
      last_name: data.last_name,
      email: data.email,
      phone_number: data.phone_number
    }
    Vue.http.post(vehicleOwnerUrls.postVehicleOwner, postData).then(function (response) {
      context.dispatch('get_all_vehicle_owners')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: error.data.message})
      // console.log(error.data)
    })
  },
  update_vehicle_owner (context, data) {
    const postData = {
      first_name: data.first_name,
      last_name: data.last_name,
      email: data.email,
      phone_number: data.phone_number
    }
    Vue.http.post(vehicleOwnerUrls.editVehicleOwner + data.public_id, postData).then(function (response) {
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: error.data.message})
      // console.log(error.data)
    })
  },
  delete_vehicle_owner (context, publicId) {
    Vue.http.get(vehicleOwnerUrls.deleteVehicleOwner + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_VEHICLE_OWNERS', response.data)
      router.push({
        name: 'Module.DeletedVehicleOwners'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreVehicleOwner (context, publicId) {
    Vue.http.get(vehicleOwnerUrls.restoreVehicleOwner + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_VEHICLE_OWNERS', response.data)
      router.push({
        name: 'Module.VehicleOwners'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
