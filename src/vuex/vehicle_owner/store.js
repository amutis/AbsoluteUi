import mutations from './mutations'
import actions from './actions'

const state = {
  all_vehicle_owners: [],
  vehicle_owners: [],
  vehicle_owner: [],
  deleted_vehicle_owners: []
}

export default {
  state, mutations, actions
}
