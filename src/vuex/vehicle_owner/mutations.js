export default {
  GET_VEHICLE_OWNERS (state, data) {
    state.vehicle_owners = data.data
  },
  GET_ALL_VEHICLE_OWNERS (state, data) {
    state.all_vehicle_owners = data.data
  },
  GET_VEHICLE_OWNER (state, data) {
    state.vehicle_owner = data.data
  },
  GET_DELETED_VEHICLE_OWNERS (state, data) {
    state.deleted_vehicle_owners = data.data
  }
}
