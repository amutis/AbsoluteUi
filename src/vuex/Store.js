import Vue from 'vue'
import Vuex from 'vuex'
import companyStore from './companies/store'
import countryStore from './countries/store'
import errorStore from './errors/store'
import userTypeStore from './user_types/store'
import userStore from './users/store'
import rideStore from './rides/store'
import rideTypeStore from './ride_types/store'
import accountLineStore from './account_lines/store'
import vehicleTypeStore from './vehicle_types/store'
import driverStore from './drivers/store'
import positionStore from './positions/store'
import ownerStore from './vehicle_owner/store'
import vehicleStore from './vehicles/store'
import assignmentStore from './assignments/store'
import contractStore from './contracts/store'
import authStore from './authentication/store'
import zoneStore from './zones/store'
import loadingStore from './loading/store'

Vue.config.productionTip = false
Vue.use(Vuex)
Vue.config.debug = true

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    companyStore,
    countryStore,
    userTypeStore,
    userStore,
    errorStore,
    rideStore,
    rideTypeStore,
    accountLineStore,
    vehicleTypeStore,
    driverStore,
    positionStore,
    vehicleStore,
    ownerStore,
    assignmentStore,
    authStore,
    contractStore,
    zoneStore,
    loadingStore
  },
  strict: debug
})
