// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import * as VueGoogleMaps from 'vue2-google-maps'
import store from './vuex/Store'
import Vuex from 'vuex'
import VueResource from 'vue-resource'
import VueGmaps from 'vue-gmaps'
import fullCalendar from 'vue-fullcalendar'
import 'vue-event-calendar/dist/style.css' // ^1.1.10, CSS has been extracted as one file, so you can easily update it.
import vueEventCalendar from 'vue-event-calendar'
import VueProgressBar from 'vue-progressbar'
import VuePaginate from 'vue-paginate'
import VueNotifications from 'vue-notifications'
import miniToastr from 'mini-toastr'
import VueGoodTable from 'vue-good-table'

Vue.use(Vuex)
Vue.use(VueGoodTable)
Vue.use(VuePaginate)
Vue.use(vueEventCalendar, {locale: 'en'})
Vue.component('full-calendar', fullCalendar)
Vue.use(VueResource)
Vue.config.productionTip = false
Vue.use(VueProgressBar, {
  color: 'rgb(143, 255, 199)',
  failedColor: 'red',
  height: '5px',
  thickness: '10px'
})
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDkY8ONBIAhgLR1I5sauEw_yhuwnKD4i4U',
    libraries: 'places, directions' // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)
  }
})
Vue.use(VueGmaps, {
  key: 'AIzaSyDyhPQ3p-isxohYBO4SREmk2-Q_XSIMhDM',
  loadGoogleApi: false
})
/* eslint-disable no-new */
Vue.http.interceptors.push(function (request, next) {
  // modify headers
  request.headers.set('Accept', 'application/json')
  // request.headers.set('Access-Control-Allow-Origin', '*')
  request.headers.set('Authorization', 'Bearer ' + localStorage.getItem('access_token'))
  // continue to next interceptor
  next()
})

// If using mini-toastr, provide additional configuration
const toastTypes = {
  success: 'success',
  error: 'error',
  info: 'info',
  warn: 'warn'
}

miniToastr.init({types: toastTypes})

// Here we setup messages output to `mini-toastr`
function toast ({title, message, type, timeout, cb}) {
  return miniToastr[type](message, title, timeout, cb)
}

// Binding for methods .success(), .error() and etc. You can specify and map your own methods here.
// Required to pipe our output to UI library (mini-toastr in example here)
// All not-specified events (types) would be piped to output in console.
const options = {
  success: toast,
  error: toast,
  info: toast,
  warn: toast
}

Vue.use(VueNotifications, options)// VueNotifications have auto install but if we want to specify options we've got to do it manually.

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
